#include <armadillo-9.850.1/include/armadillo>
#pragma once

using namespace arma;
using namespace std;

class Ambient
{
private:
	vec m_newVec;
	double m_direction;

public :
	Ambient();

	Ambient(vec _c, double _dir);

	void SetDirection(double _dir) 
	{
		m_direction = _dir;
	}

	double GetDirection() const 
	{
		return m_direction;
	}

	void SetVec(vec _vecToSet) 
	{
		m_newVec = _vecToSet;
	}

	vec GetVec() const 
	{
		return m_newVec;
	}
};

