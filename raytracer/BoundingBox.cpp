#include "BoundingBox.h"

BoundingBox::BoundingBox(){}

bool BoundingBox::Intersect(HitPoint& _hitPoint)
{
	double tmin, tmax, tymin, tymax, tzmin, tzmax;

	// set thje bounds for tmin and max
	tmin = (m_bounds[_hitPoint.GetSign()[0]](0) - _hitPoint.GetOrigin()(0)) * _hitPoint.GetInv_d()(0);
	tmax = (m_bounds[1 - _hitPoint.GetSign()[0]](0) - _hitPoint.GetOrigin()(0)) * _hitPoint.GetInv_d()(0);

	// set the bounds on the tymin and max
	tymin = (m_bounds[_hitPoint.GetSign()[1]](1) - _hitPoint.GetOrigin()(1)) * _hitPoint.GetInv_d()(1);
	tymax = (m_bounds[1 - _hitPoint.GetSign()[1]](1) - _hitPoint.GetOrigin()(1)) * _hitPoint.GetInv_d()(1);

	// if any of our mins are larger than our maxes then we have failed
	if (tmin > tymax || tymin > tmax) 
		return false;

	if (tymin > tmin)
		tmin = tymin;

	if (tymax < tmax)
		tmax = tymax;

	// set the bounds for z axis bounds
	tzmin = (m_bounds[_hitPoint.GetSign()[2]](2) - _hitPoint.GetOrigin()(2)) * _hitPoint.GetInv_d()(2);
	tzmax = (m_bounds[1 - _hitPoint.GetSign()[2]](2) - _hitPoint.GetOrigin()(2)) * _hitPoint.GetInv_d()(2);

	if (tmin > tzmax || tzmin > tmax)
		return false;

	if (tzmin > tmin)
		tmin = tzmin;

	if (tzmax < tmax)
		tmax = tzmax;

	if (tmin > _hitPoint.GetTmin())
		_hitPoint.SetTmin(tmin);

	if (tmax < _hitPoint.GetTmax())
		_hitPoint.SetTMax(tmax);

	return true;
}
