#include <armadillo-9.850.1/include/armadillo>
#include <vector>
#include "renderable.h"
#pragma once

using namespace arma;
using namespace std;

class BoundingBox
{
public:
	std::vector<vec> m_bounds;

	BoundingBox();

	BoundingBox(vec _min, vec _max)
	{
		m_bounds.push_back(_min);
		m_bounds.push_back(_max);
	}

	vec GetMin()
	{
		return m_bounds[0];
	}

	vec GetMax()
	{
		return m_bounds[1];
	}

	bool Intersect(HitPoint& _hitpoint);
};

