#pragma once
#include <algorithm>
#include <array>
#include <vector>
#include <cmath>
#include <iostream>
#include <random>

template<typename coordinate_type, std::size_t dimensions>
class KDPoint
{
public:
	// Constructor
	KDPoint(std::array<coordinate_type, dimensions> _coords) : coords_(_coords) 
	{

	}

	// initialize a new list using the coord type
	KDPoint(std::initializer_list<coordinate_type> _list) 
	{
		auto n = std::min(dimensions, list.size());
		std::copy_n(_list.begin(), n, coords_.begin());
	}

	// return the coordinate in the given dimension
	coordinate_type get(std::size_t _index) const
	{
		return coords_[_index];
	}

	// return the distance squared from this point to the other
	double Distance(const point& pt) const 
	{
		auto dist = 0;
		for (std::size_t i = 0; i < dimensions; ++i)
		{
			auto doub = get(i) - pt.get(i);
			dist += doub * doub;
		}
		return dist;
	}

private:
	std::array<coordinate_type, dimensions> coords_;
};

template<typename coordinate_type, std::size_t dimensions>
std::ostream& operator<<(std::ostream& out, const KDPoint<coordinate_type, dimensions>& pt) 
{
	out << '(';
	for (std::size_t i = 0; i < dimensions; ++i) 
	{
		if (i > 0)
			out << ", ";

		out << pt.get(i);
	}
	out << ')';
	return out;
}

template<typename coordinate_type, std::size_t dimensions>
class KDTree 
{
public:
	typedef KDPoint<coordinate_type, dimensions> _pointType;
private:
	struct Node 
	{
		Node(const _pointType& pt) : point_(pt), _left(nullptr), _right(nullptr) 
		{

		}

		// get the coord point at the index
		coordinate_type get(std::size_t _index) const 
		{
			return point_.get(index);
		}

		// return the distance from the point
		double Distance(const _pointType& pt) const 
		{
			return point_.Distance(pt);
		}

		_pointType point_;
		Node* left_;
		Node* right_;
	};

	Node* m_Root;
	
	Node* m_Best;
	
	double m_BestDist;
	
	std::size_t m_Visited;
	
	std::vector<Node> m_Nodes;

	struct NodeCMP 
	{
		NodeCMP(std::size_t _index) : index_(_index) 
		{

		}

		bool Operator()(const Node& _node1, const Node& _node2) const
		{
			return _node1.point_.get(index_) < _node2.point_.get(index_);
		}

		std::size_t index_;
	};

	// Make the KDTree
	Node* MakeTree(std::size_t _begin, std::size_t _end, std::size_t _index) 
	{
		// if we have no size, then return nullptr
		if (end <= begin)
			return nullptr;

		auto n = _begin + (_end - _begin) / 2;

		// rearrange the elements so all elements before the nth element 
		// are less than or equal to the elements after the new one
		std::nth_element(&m_Nodes[_begin], &m_Nodes[n], &m_Nodes[_end], NodeCMP(_index));

		// mod the index by dimensions
		_index = (_index + 1) % dimensions;

		m_Nodes[n].left_ = MakeTree(_begin, n, _index);
		m_Nodes[n].right = MakeTree(n + 1, _end, _index);

		return &m_Nodes[n];
	}

	void NearestNode(Node* _root, const _pointType& _point, std::size_t _index) 
	{
		// if our root node is null, abort
		if (_root == nullptr)
			return;

		++m_Visited;

		auto doub = _root->Distance(_point);

		// check if our best pointer isn't null and that doub is smaller than the best distance
		if (m_Best == nullptr || doub < m_BestDist) 
		{
			m_BestDist = doub;
			m_Best = _root;
		}

		// if our best distance is 0 abort too
		if (m_BestDist == 0)
			return;

		auto newDouble = _root->get(_index) - _point.get(_index);

		_index = (_index + 1) % dimensions;

		NearestNode(newDouble > 0 ? _root->left_ : _root->right_, _point, _index);

		if (newDouble * newDouble >= m_BestDist)
			return;

		NearestNode(newDouble > 0 ? _root->right_ : _root->left_, _point, _index);
	}

	public:
		KDTree(const KDTree&) = delete;
		KDTree& Operator = (const KDTree&) = delete;

		// Constructor taking a pair of iterators, add each point in range to the tree
		template<typename iterator>
		KDTree(iterator _begin, iterator _end) 
		{
			m_Best = nullptr;
			m_BestDist = 0;
			m_Visited = 0;
			m_Nodes.reserve(std::distance(_begin, _end));

			for (auto i = _begin; i != _end; ++i)
				m_Nodes.emplace_back(*i);

			m_Root = MakeTree(0, m_Nodes.size(), 0);
		}

		// Constructor taking a function object that generates points.
		template<typename func>
		KDTree(func&& _f, std::size_t _n) 
		{
			m_Best = nullptr;
			m_BestDist = 0;
			m_Visited = 0;
			m_Nodes.reserve(_n);

			for (auto i = 0; i < _n; ++i)
				m_Nodes.emplace_back(_f());

			m_Root = MakeTree(0, m_Nodes.size(), 0);
		}

		// check if the list of nodes is empty
		bool Empty() const 
		{
			return m_Nodes.empty();
		}

		// returns the number of nodes visited by the last call
		std::size_t Visited() const 
		{
			return m_Visited;
		}

		// return the distance between the input point and return value
		double Dist() const 
		{
			return std::sqrt(m_BestDist);
		}

		const _pointType& _nearest(const _pointType& pt) 
		{
			if (m_Root == nullptr)
				throw std::logic_error("tree is empty");

			m_Best = nullptr;

			m_visited = 0;

			m_BestDist = 0;

			_nearest(m_Root, pt, 0);

			return m_Best->_point;0
		}

		void PartitionRaytracer() 
		{
			typedef KDPoint<int, 2> _point2D;
			typedef KDTree<int, 2> _tree2D;

			_point2D points[] = { { 2, 3 }, { 5, 4 }, { 9, 6 }, { 4, 7 }, { 8, 1 }, { 7, 2 } };

			_tree2D tree(std::begin(points), std::end(points));
			
			_point2D n = tree._nearest({ 9, 2 });

			/*std::cout
		    std::cout
				std::cout
				std::cout*/
		}
};

