#include "PartitioningNode.h"

void PartitioningNode::AddObject(HitPoint* _hitPoint)
{
	// check if we already contain children
	if (m_ChildNodes.size() > 0)
	{
		// loop over all children
		for (auto child : m_ChildNodes)
		{
			if (child->m_BoundingBox.Intersects(_hitPoint->m_Bounds))
				child->AddObject(_hitPoint);
		}
	}
	// we have no children
	else
	{
		// add the tile
		m_NodeContents.push_back(_hitPoint);

		// check if need to split
		if (SplitCheck())
		{
			// split
			auto min = Vector2f::Zero;
			auto max = Vector2f::Zero;

			// create the children
			auto topLeftChild = new PartitioningNode();

			// set min/max values
			min = Vector2f(m_BoundingBox.boxMin.X, m_BoundingBox.Centre().Y);
			max = Vector2f(m_BoundingBox.Centre().X, m_BoundingBox.boxMax.Y);

			// Set bounding box and current level in tree for topLeftChild
			topLeftChild->m_BoundingBox = AABBf(min, max);
			topLeftChild->m_CurrentLvlInTree = m_CurrentLvlInTree + 1;

			// Add the top left node to children
			m_ChildNodes.push_back(topLeftChild);

			// Repeat for all other nodes

				// topRightChild Node
			auto topRightChild = new PartitioningNode();

			// set min/max values
			min = m_BoundingBox.Centre();
			max = m_BoundingBox.boxMax;

			// Set bounding box and current level in tree for topRightChild
			topRightChild->m_BoundingBox = AABBf(min, max);
			topRightChild->m_CurrentLvlInTree = m_CurrentLvlInTree + 1;

			// Add the top right node to children
			m_ChildNodes.push_back(topRightChild);

			// bottomLeftChild Node
			auto bottomLeftChild = new PartitioningNode();

			// set min/max values
			min = m_BoundingBox.boxMin;
			max = m_BoundingBox.Centre();

			// Set bounding box and current level in tree for bottomLeftChild
			bottomLeftChild->m_BoundingBox = AABBf(min, max);
			bottomLeftChild->m_CurrentLvlInTree = m_CurrentLvlInTree + 1;

			// Add the bottomLeftNode to children
			m_ChildNodes.push_back(bottomLeftChild);

			// bottomRightChild node
			auto bottomRightChild = new PartitioningNode();

			// set min/max values
			min = Vector2f(m_BoundingBox.Centre().X, m_BoundingBox.boxMin.Y);
			max = Vector2f(m_BoundingBox.boxMax.X, m_BoundingBox.Centre().Y);

			// Set bounding box and current level in tree for bottomRightChild
			bottomRightChild->m_BoundingBox = AABBf(min, max);
			bottomRightChild->m_CurrentLvlInTree = m_CurrentLvlInTree + 1;

			// Add the bottomRightNode to children
			m_ChildNodes.push_back(bottomRightChild);

			// loop over children
			for (auto child : m_ChildNodes)
			{
				// loop over all the objects
				for (auto _myHitPoint : m_NodeContents)
				{
					// if they overlap then add them to the child
					if (child->m_boundingBox.Intersects(_myHitPoint->m_Bounds))
						child->AddObject(_myHitPoint);
				}
			}
		}
	}

}

bool PartitioningNode::SplitCheck() 
{
	// if we are empty we don't need to split
	if (m_NodeContents.empty())
		return false;
	
	// otherwise we need to check if we contain more than our min split size, if we have then we need to split!
	else if (m_NodeContents.size() >= m_MinObjSplitSize && (m_BoundingBox.Width() >= m_MinObjSplitSize && m_BoundingBox.Height() >= m_MinObjSplitSize))
		return true;

	return false;
}