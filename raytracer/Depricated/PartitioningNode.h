#pragma once
#include "AABB.h"
#include "renderable.h"
#include <vector>


class PartitioningNode
{
public:
	AABBf m_BoundingBox;

	PartitioningNode* m_Parent;

	std::vector<PartitioningNode*> m_ChildNodes;
	std::vector<HitPoint*> m_NodeContents;

	int m_CurrentLvlInTree = -1;

	int m_MinSplitSize = 3;
	int m_MinObjSplitSize = 5;

	// Set m_Parent to point at nothing
	PartitioningNode()
	{
		m_Parent = nullptr;
	}

	// set m_Parent to be the new node
	PartitioningNode(PartitioningNode* _parent)
	{
		m_Parent = _parent;
	}

	// Once we are done with the nodes, clear the pointers
	~PartitioningNode()
	{
		for (PartitioningNode* nodePtr : m_ChildNodes)
			delete nodePtr;

		m_ChildNodes.clear();

		for (HitPoint* hitPointPtr : m_NodeContents)
			delete hitPointPtr;

		m_NodeContents.clear();
	}

	// Add Hits to the m_NodeContes <vector>
	void AddObject(HitPoint* _hitPoint);

	// Check if we need to split
	bool SplitCheck();
};