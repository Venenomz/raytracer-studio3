#include "HeapSort.h"
#include <armadillo-9.850.1/include/armadillo>
#include <vector>

using namespace arma;
using namespace std;

HeapSort::HeapSort() {}

vector <Renderable*> HeapSort::SortObjects(vector<Renderable*>& _objects, int _n, int _axis)
{
	int i = (_n / 2) + 1, parent, child;
	Renderable* t;

	for (;;)
	{
		if (i > 0)
		{
			--i;
			t = _objects[i];
		}
		else
		{
			--_n;

			if (_n == 0)
			{
				return _objects;
			}

			t = _objects[_n];

			// set our current object to be first in the list
			_objects[_n] = _objects[0];
		}

		parent = i;
		child = i * 2;

		while (child < _n) 
		{
			if ((child + 1 < _n) && (_objects[child + 1]->GetCenter()(_axis)) > _objects[child]->GetCenter()((_axis)))
				++child;

			if (_objects[child]->GetCenter()(_axis) > t->GetCenter()(_axis)) 
			{
				_objects[parent] = _objects[child];
				parent = child;
				child = parent * 2 + 1;
			}
			else 
			{
				break;
			}
		}
		_objects[parent] = t;
	}
}


