#include "renderable.h"
#include <vector>
#pragma once

using namespace std;

class HeapSort
{
public:

	HeapSort();

	static vector<Renderable*> SortObjects(vector <Renderable*>& _allObjects, int _size, int _axis);
};

