#include <armadillo-9.850.1/include/armadillo>
#pragma once

using namespace arma;
using namespace std;

class Hittable
{
public:
	virtual vec GetMin() = 0;
	virtual vec GetMax() = 0;
	virtual vec GetCenter() = 0;
};

