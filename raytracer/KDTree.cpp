#include "KDTree.h"
#include <armadillo-9.850.1/include/armadillo>
#include <vector>
#include "HeapSort.h"

using namespace arma;
using namespace std;

// Check if the current tree is in the leaf
bool KDTree::Leaf(KDTree*& _treeToCheck) 
{
	if (_treeToCheck->GetRightSide() == NULL && _treeToCheck->GetLeftSide() == NULL)
		return true;

	return false;
}

// Create the tree
KDTree* KDTree::ConstructTree(vector<Renderable*>& _objects, const unsigned int& _size, int _level)
{
	if (_objects.size()==0)
		return 0;

	// create new trees and renderable objects
	KDTree* newTree = new KDTree();
	vector <Renderable*> right, left;

	vec min, max;

	BoundingBox bounds;

	// set the max and min on the bounds
	max = _objects[0]->GetMax();
	min = _objects[0]->GetMin();

	// Setup bounding box variables
	for (unsigned int i = 1; i < _objects.size(); ++i)
	{
		if (_objects[i]->GetMin()(0) < min(0)) 
			min(0) = _objects[i]->GetMin()(0); 

		if (_objects[i]->GetMin()(1) < min(1))  
			min(1) = _objects[i]->GetMin()(1); 

		if (_objects[i]->GetMin()(2) < min(2)) 
			min(2) = _objects[i]->GetMin()(2);

		if (_objects[i]->GetMax()(0) > max(0))
			max(0) = _objects[i]->GetMax()(0); 

		if (_objects[i]->GetMax()(1) > max(1))  
			max(1) = _objects[i]->GetMax()(1);

		if (_objects[i]->GetMax()(2) > max(2))  
			max(2) = _objects[i]->GetMax()(2); 
	}

	// Set up bounds, objects and axis for the new tree
	newTree->SetBounds(BoundingBox(min, max));
	newTree->SetObject(_objects);
	newTree->SetAxis(_level % 3);
	//return newTree;

	// Check if we are too large
	if (_level >= 20 || _objects.size() < _size)
	{
		newTree->SetRight(NULL);
		newTree->SetLeft(NULL);

		return newTree;
	}

	// sort all the objects
	auto xx = HeapSort::SortObjects(_objects, _objects.size(), newTree->GetAxis());

	// Set the split for the tree using the objects axis
	newTree->SetSplit(xx[xx.size() / 2]->GetMin()(newTree->GetAxis())-0.01);

	// create splits
	if (xx.size() % 2 == 1)
	{
		newTree->SetSplit(newTree->GetSplit() + xx[(xx.size() / 2) - 1]->GetMin()(newTree->GetAxis()) - 0.01);
		//newTree->SetSplit(newTree->GetSplit() / 2);
	}

	// Loop through the objects and get the axis's and split points
	for (unsigned int w = 0; w < xx.size(); ++w)
	{
		// if we are on the left side of the split, push to the left tree
		if ((xx[w]->GetMin()(newTree->GetAxis()) <= newTree->GetSplit()) && (xx[w]->GetMax()(newTree->GetAxis()) <= newTree->GetSplit()))
		{
			left.push_back(xx[w]);
			xx.erase(xx.begin() + w);
			--w;
		}
		// if we are on the right side of the split, push to the right tree
		else if ((xx[w]->GetMin()(newTree->GetAxis()) > newTree->GetSplit()) && (xx[w]->GetMax()(newTree->GetAxis()) > newTree->GetSplit()))
		{
			right.push_back(xx[w]);
			xx.erase(xx.begin() + w);
			--w;
		}
		// otherwise, push to all trees
		else
		{
			//right.push_back(xx[w]);
			//left.push_back(xx[w]);
		}
	}
	// Set up te left and right tree
	newTree->SetRight(KDTree::ConstructTree(right, _size, _level + 1));
	newTree->SetLeft(KDTree::ConstructTree(left, _size, _level + 1));
	newTree->m_hittableObjs = xx;
	//newTree->m_hittableObjs.clear();
	return newTree;

}
// browse the current tree to search for collisions
KDTree* KDTree::BrowseTree(KDTree*& _newTree, HitPoint &_point, bool& _collide, double& _tmin, double& _size, int& _index)
{
	// get the current tree
	KDTree* currentTree = _newTree;

	vector<KDTree*> stack;
	int levelInStack = 0;

	// while we aren't finished
	while (levelInStack != -1)
	{
		// if the current tree is not in the leaf
		if (!Leaf(currentTree))
		{
			HitPoint r = _point, r1 = r;

			// find where we intersect
			auto intersect1 = false;
			auto intersect2 = false;

			if (currentTree->GetRightSide())
				intersect1 = currentTree->GetRightSide()->GetBounds().Intersect(r);

			if (currentTree->GetLeftSide())
				intersect2 = currentTree->GetLeftSide()->GetBounds().Intersect(r1);

			// check for a double intersect
			if (intersect1 && intersect2)
			{
				// If r1 is larger than r then push to the right side of the stack
				if (r1.GetTmin() < r.GetTmin())
				{
					stack.push_back(currentTree->GetRightSide());
					currentTree = currentTree->GetLeftSide();
				}
				// otherwise push to the left side
				else
				{
					stack.push_back(currentTree->GetLeftSide());
					currentTree = currentTree->GetRightSide();
				}

				++levelInStack;
			}

			// if there is only one intersect, then get the right side
			else if (intersect1)
				currentTree = currentTree->GetRightSide();

			// if there is only one intersect, then get the left side
			else if (intersect2)
				currentTree = currentTree->GetLeftSide();
			
			// if there is no intersect, then we must be done!
			else
			{
				--levelInStack;

				if (levelInStack != -1)
				{
					currentTree = stack[levelInStack];
					stack.resize(levelInStack);
				}
			}
		}
		// otherwise we are in the leaf, so we need to do some more calculations
		else
		{
			// check all objects in the current tree
			for (unsigned int i = 0; i < currentTree->GetObjects().size(); i++) 
			{
				// check if the objects collide with the ray
				if (currentTree->GetObjects()[i]->Collide(_point.GetD(), _size, _point.GetOrigin()))
				{
					_collide = true;

					// do size stuff
					if (_size >= 0 && _size <= _tmin) 
					{
						_tmin = _size;
						_index = i;
					}
				}
			}

			// if there is a collision, then return the current tree
			if (_collide)
				return currentTree;

			// otherwise, we must be done so end
			else 
			{
				levelInStack--;

				if (levelInStack != -1) 
				{
					currentTree = stack[levelInStack];
					stack.resize(levelInStack);
				}
			}
		}
	}

	// we need the tree, so return it
	return currentTree;
}
extern int countthem;

HitPoint KDTree::BrowseTreeG(KDTree*& _newTree, HitPoint _point, bool& _collide, double& _tmin, double& _size, int& _index)
{
	for (int i = 0; i < _newTree->m_hittableObjs.size(); ++i)
	{
		countthem++;
		HitPoint hp = _newTree->m_hittableObjs[i]->intersect(_point.m_ray);
		if (hp.m_hit)
		{
			int dummmy = 0;
		}
		_point.nearest(hp);
	}
	if (_newTree->m_left)
	{
		Sphere s;
		Box b;
		vec centre = _newTree->m_left->GetCenter();
		vec size = _newTree->m_left->GetMax() - _newTree->m_left->GetMin();
		s.m_position.set(centre(0), centre(1), centre(2));
		s.m_radius = sqrt(size(0) * size(0) + size(1) * size(1) + size(2) * size(2));
		b.m_position.set(centre(0), centre(1), centre(2));
		b.m_size.set(size(0), size(1), size(2));
		if (b.intersect(_point.m_ray).m_hit)
		{
			_point.nearest(BrowseTreeG(_newTree->m_left, _point, _collide, _tmin, _size, _index));
		}

	}
	if (_newTree->m_right)
	{
		Sphere s;
		Box b;
		vec centre = _newTree->m_right->GetCenter();
		vec size = _newTree->m_right->GetMax() - _newTree->m_right->GetMin();
		s.m_position.set(centre(0), centre(1), centre(2));
		s.m_radius = sqrt(size(0) * size(0) + size(1) * size(1) + size(2) * size(2));
		b.m_position.set(centre(0), centre(1), centre(2));
		b.m_size.set(size(0), size(1), size(2));
		if (b.intersect(_point.m_ray).m_hit)
		{
			_point.nearest(BrowseTreeG(_newTree->m_right, _point, _collide, _tmin, _size, _index));
		}

	}
	return _point;
}

std::string indent(int level)
{
	std::string s;
	for (int i = 0; i < level; ++i)
	{
		s += " ";
	}
	return s;
}

void KDTree::dump(std::fstream& file, int level)
{
	file << indent(level) << "{\n";
	file << indent(level + 1) << "\"count\" = " << m_hittableObjs.size() << ((m_left!=0 || m_right!=0)?",":"")<<"\n";
	file << indent(level + 1) << "<" << m_bounds.GetMin()(0) << "," << m_bounds.GetMin()(1) << "," << m_bounds.GetMin()(2) << ">"<< "\n";
	file << indent(level + 1) << "<" << m_bounds.GetMax()(0) << "," << m_bounds.GetMax()(1) << "," << m_bounds.GetMax()(2) << ">"<< "\n";
	file << indent(level + 1) << "[\n";
		for (int i = 0; i < m_hittableObjs.size(); ++i)
	{
		Sphere* s;
		Box* b;
		b = dynamic_cast<Box*>(m_hittableObjs[i]);
		s = dynamic_cast<Sphere*>(m_hittableObjs[i]);
		file << indent(level + 1) << "{\n";
		if (s)
		{
			file << indent(level + 1) << "\"radius\" = " << s->m_radius << ",\n";
			file << indent(level + 1) << "\"x\" = " << s->m_position.x << ",\n";
			file << indent(level + 1) << "\"y\" = " << s->m_position.y << ",\n";
			file << indent(level + 1) << "\"z\" = " << s->m_position.z << "\n";
		}
		if (b)
		{
			file << indent(level + 1) << "\"size\" = \"" << b->m_size.x << ","<< b->m_size.y << ","<< b->m_size.z << ","<<"\",\n";
			file << indent(level + 1) << "\"x\" = " << b->m_position.x << ",\n";
			file << indent(level + 1) << "\"y\" = " << b->m_position.y << ",\n";
			file << indent(level + 1) << "\"z\" = " << b->m_position.z << "\n";
		}
		file << indent(level + 1) << "}\n";
	}
		file << indent(level + 1) << "]\n";
		if (m_left)
	{
		file << indent(level + 1) << "\"left\" = \n";
		m_left->dump(file, level + 1);
	}
	if (m_right)
	{
		file << indent(level + 1) << "\"right\" = \n";
		m_right->dump(file, level + 1);
	}
	file << indent(level) << "}\n";
}
