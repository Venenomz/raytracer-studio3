#include <armadillo-9.850.1/include/armadillo>
#include "light.h"
#include "renderable.h"
#include "BoundingBox.h"
#include "hittable.h"
#include "kf/kf_ray.h"
#pragma once


using namespace arma;
using namespace std;

class KDTree :public Hittable
{
private:
	BoundingBox m_bounds;
	KDTree* m_left, *m_right;
	int m_axis;
	double m_split;
	vector<Renderable*> m_hittableObjs;
	kf::Ray m_ray;

public:
	vec GetCenter() 
	{
		return (m_bounds.GetMax()+m_bounds.GetMin())*0.5;
	}

	vec GetMin() 
	{
		return m_bounds.GetMin();
	}

	vec GetMax() 
	{
		return m_bounds.GetMax();
	}

	kf::Ray GetRay() 
	{
		return m_ray;
	}

	kf::Ray SetRay(kf::Ray _raycast) 
	{
		m_ray = _raycast;
	}

	static KDTree* ConstructTree(vector <Renderable*>& _objects, const unsigned int& _size, int _level = 0);
	static KDTree* BrowseTree(KDTree*& _treeToCheck, HitPoint& _hitPoint, bool& _collide, double& _tmin, double& _t, int& index);
	static HitPoint BrowseTreeG(KDTree*& _treeToCheck, HitPoint _hitPoint, bool& _collide, double& _tmin, double& _t, int& index);

	static bool Leaf(KDTree*& _treeToCheck);

	void SetObject(vector<Renderable*> _objects) 
	{
		m_hittableObjs = _objects;
	}

	vector<Renderable*> GetObjects() 
	{
		return m_hittableObjs;
	}

	void SetSplit(double _split) 
	{
		m_split = _split;
	}

	double GetSplit() 
	{
		return m_split;
	}

	void SetAxis(int _axis) 
	{
		m_axis = _axis;
	}

	int GetAxis() 
	{
		return m_axis;
	}

	void SetLeft(KDTree* _tree) 
	{
		m_left = _tree;
	}

	KDTree* GetLeftSide() 
	{
		return m_left;
	}

	void SetRight(KDTree* _tree) 
	{
		m_right = _tree;
	}

	KDTree* GetRightSide() 
	{
		return m_right;
	}

	void SetBounds(BoundingBox _bounds) 
	{
		m_bounds = _bounds;
	}

	BoundingBox GetBounds() 
	{
		return m_bounds;
	}

	~KDTree() 
	{
		for (size_t i = 0; i < m_hittableObjs.size(); i++)
			delete m_hittableObjs[i];

		m_hittableObjs.clear();

		m_left = NULL;

		m_right = NULL;
	}

	void dump(std::fstream& file, int level);
};


