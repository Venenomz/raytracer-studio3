#include "renderable.h"
#include <algorithm>

void HitPoint::nearest(const HitPoint &hp)
{
	if (!m_hit)
		*this = hp;
	else
	{
		if (hp.m_hit && hp.m_distance < m_distance)
		{
		//	kf::Ray r = m_ray;
			*this = hp;
		//	m_ray = r;
		}
	}
}

Renderable::Renderable()
{
	
}

vec Sphere::GetMin() 
{
	vec t;
	
	t << m_position.x-m_radius << m_position.y - m_radius << m_position.z - m_radius;
	return t;
}

vec Sphere::GetCenter()
{
	vec t;
	t << m_position.x << m_position.y << m_position.z;

	return t;
}

vec Sphere::GetMax()
{
	vec t;
	t << m_position.x + m_radius << m_position.y + m_radius << m_position.z + m_radius;
	return t;
}

vec Box::GetMin()
{
	vec t;
	t << m_position.x - m_size.x << m_position.y - m_size.y << m_position.z - m_size.z;
	return t;
}

vec Box::GetCenter()
{
	vec t;
	t << m_position.x << m_position.y << m_position.z;
	return t;
}

vec Box::GetMax()
{
	vec t;
	t << m_position.x + m_size.x << m_position.y + m_size.y << m_position.z + m_size.z;
	return t;
}

Sphere::Sphere(float rad) :m_radius(rad)
{

}

HitPoint Sphere::intersect(const kf::Ray &ray)
{
	vec rayStart, rayDir;

	rayStart << ray.start().x << ray.start().y << ray.start().z;

	rayDir << ray.delta().x << ray.delta().y << ray.delta().z;

	HitPoint hp(rayStart, rayDir);
	hp.m_ray = ray;
	kf::Vector3 L = m_position-(ray.start());

	float tca = L.dot(ray.delta()); 

	if (tca < 0) 
		return hp; 

	float d2 = L.dot(L) - tca * tca; 

	if (d2 > m_radius*m_radius) 
		return hp; 

	float thc = sqrt(m_radius*m_radius - d2);

	float t0 = tca - thc; 

	float t1 = tca + thc;

	float m = std::min(t0, t1);

	hp.m_hit = true;

	hp.m_position = ray.interpolate(m);

	hp.m_renderable = this;

	hp.m_normal = normalise(hp.m_position-m_position);

	hp.m_distance = m;

	return hp;
}

Box::Box(float w, float h, float d) : m_size(w,h,d)
{
}

HitPoint Box::intersect(const kf::Ray &ray)
{
	vec rayStart, rayDir;

	rayStart << ray.start().x << ray.start().y << ray.start().z;

	rayDir << ray.delta().x << ray.delta().y << ray.delta().z;

	HitPoint hp(rayStart, rayDir);

	kf::Vector3 minCorner(m_position - m_size);

	kf::Vector3 maxCorner(m_position + m_size);

	kf::Vector3 inv = 1.0f / ray.delta();

	double t1 = (minCorner.x - ray.start().x) * inv.x;

	double t2 = (maxCorner.x - ray.start().x) * inv.x;

	double tmin = std::min(t1, t2);

	double tmax = std::max(t1, t2);

	for (int i = 1; i < 3; ++i)
	{
		t1 = (minCorner[i] - ray.start()[i]) * inv[i];

		t2 = (maxCorner[i] - ray.start()[i]) * inv[i];

		tmin = std::max(tmin, std::min(std::min(t1, t2), tmax));

		tmax = std::min(tmax, std::max(std::max(t1, t2), tmin));
	}

	if(tmax > std::max(tmin, 0.0))
	{
		hp.m_hit = true;

		hp.m_position = ray.interpolate(tmin);

		hp.m_renderable = this;

		kf::Vector3 p = ((hp.m_position-m_position) / m_size)*1.00001f;

		if (abs(p.x) >= 1)
			hp.m_normal.set(p.x, 0, 0);

		else if (abs(p.y) >= 1)
			hp.m_normal.set(0, p.y, 0);

		else 
			hp.m_normal.set(0, 0, p.z);

		//hp.m_normal.set(int(p.x),int(p.y),int(p.z));
		//hp.m_normal.normalise();

		hp.m_distance = tmin;
	}

	return hp;
}

