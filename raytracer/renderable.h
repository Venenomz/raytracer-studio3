#ifndef RENDERABLE_H
#define RENDERABLE_H
#include <vector>
#include "kf/kf_vector.h"
#include "kf/kf_ray.h"
#include "material.h"
#include <armadillo-9.850.1/include/armadillo>
#include "light.h"
#include "Ambient.h"
#include "Hittable.h"

using namespace arma;

class Renderable;

class HitPoint
{
private:
	vec m_origin, m_d, m_invd;

	double m_tmax, m_tmin;

	int m_sign[3];

public:

	HitPoint(vec _origin, vec _d) :m_distance(0), m_hit(false), m_renderable(0)
	{
		m_origin = _origin;
		m_d = _d;
		m_invd = 1 / _d;
		m_tmin = std::numeric_limits<double>::min();
		m_tmax = std::numeric_limits<double>::max();
		m_sign[0] = (m_invd(0) < 0);
		m_sign[1] = (m_invd(1) < 0);
		m_sign[2] = (m_invd(2) < 0);
	}

	HitPoint() :m_distance(0), m_hit(false), m_renderable(0)
	{
		m_origin << 0 << 0 << 0;
		m_d << 0 << 0 << 0;
		m_invd << 0 << 0 << 0;
		m_sign[0] = 0;
		m_sign[1] = 0;
		m_sign[2] = 0;

	}

	void SetTMax(double _tmax)
	{
		m_tmax = _tmax;
	}

	double GetTmax() const
	{
		return m_tmax;
	}

	void SetTmin(double _tmin)
	{
		m_tmin = _tmin;
	}

	double GetTmin() const
	{
		return m_tmin;
	}

	void SetInv_d(vec _invd)
	{
		m_invd = _invd;
	}

	vec GetInv_d() const
	{
		return m_invd;
	}

	void SetD(vec _d) 
	{
		m_d = _d;
	}

	vec GetD() const 
	{
		return m_d;
	}

	void SetOrigin(vec _origin) 
	{
		m_origin = _origin;
	}

	vec GetOrigin() const 
	{
		return m_origin;
	}

	void SetSign(int _sign[3]) 
	{
		m_sign[0] = _sign[0];
		m_sign[1] = _sign[1];
		m_sign[2] = _sign[2];
	}

	int* GetSign()
	{
		return m_sign;
	}


	void nearest(const HitPoint& hp);

	kf::Vector3 m_normal;

	kf::Vector3 m_tangent;

	kf::Vector3 m_binormal;

	kf::Vector3 m_position;

	kf::Ray m_ray;

	float m_distance;

	bool m_hit;

	Renderable* m_renderable;
};

class Renderable :public Hittable
{
private:
	vec m_kd, m_ks, m_normal;
	mat m_location;
	double m_p;
	int m_type;

public:
	Renderable();

	void SetType(int _type) 
	{
		m_type = _type;
	}

	int GetType() const 
	{
		return m_type;
	}

	void SetP(double _p) 
	{
		m_p = _p;
	}

	double GetP() const
	{
		return m_p;
	}

	void SetLocation(mat _location) 
	{
		m_location = _location;
	}

	mat GetLocation() const 
	{
		return m_location;
	}

	void SetNormal(vec _normal) 
	{
		m_normal = _normal;
	}

	vec GetNormal() const 
	{
		return m_normal;
	}

	void SetKs(vec _ks) 
	{
		m_ks = _ks;
	}

	vec GetKs() const 
	{
		return m_ks;
	}

	void SetKD(vec _kd) 
	{
		m_kd = _kd;
	}

	vec GetKD() const 
	{
		return m_kd;
	}

	virtual bool Collide(const vec& _d, double& _t2, const vec& _origin) 
	{
		return false;
	}

	virtual vec GetA() 
	{
		return 0;
	}

	virtual vec GetB() 
	{
		return 0;
	}

	virtual vec GetC() 
	{
		return 0;
	}

	vec Colour(const vec& _d, const vector<Light>& _light, const vector<Renderable*> _objects, const Ambient _ambient, const vec& _vector, const unsigned int& _index);

	virtual HitPoint intersect(const kf::Ray& ray) = 0;

	kf::Vector3 m_position;

	Material m_material;
};

class Sphere : public Renderable
{
public:
	Sphere(float rad = 1.0f);
	HitPoint intersect(const kf::Ray& ray);
	float m_radius;

	vec GetMin();
	vec GetMax();
	vec GetCenter();
};

class Box : public Renderable
{
public:
	Box(float w = 1.0f, float h = 1.0f, float d = 1.0f);
	HitPoint intersect(const kf::Ray& ray);
	kf::Vector3 m_size;

	vec GetMin();
	vec GetMax();
	vec GetCenter();
};

#endif
